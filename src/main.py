"""
    main.py - The core software

    Manage a cuckoo clock with the following sensors & actuators attached :
    - 1 contact sensor on the Gong
    - 1 servo actuator to block or restart the pendulum
    - Stops the clock after predefined number of gongs, and restart the pendulum 12h later.
    - Restart it at the exact time from Internet, to synchronize it every morning with the time from Internet.

    Features :
    - Provides on/off for the pendulum

    Tested on TTGO T-Display based on ESP32 with MicroPython 1.13 firmware compiled from sources with
    LCD color screen support. (st7789 driver from https://github.com/devbis/st7789_mpy)

    Copyright (C) 2020 François Delpierre

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

    Links:
    Datasheet : https://www.espressif.com/sites/default/files/documentation/esp32_datasheet_en.pdf
    Pinout : https://ae01.alicdn.com/kf/HTB1GyxIefWG3KVjSZFgq6zTspXas.jpg
    Wake UP from deepsleep : https://randomnerdtutorials.com/micropython-esp32-deep-sleep-wake-up-sources/

    Problems I faced:
    - BlynkLib fails to instanciate if in the if __main__ section.
    - rtc.memory() trick to save states between reset on ESP32 not easy to find (missing docs)
    - soft_reset does not reset when triggered from code ? Might be related to my pymakr
    - Wake up from deepsleep with 2 sources configured ()
    - If the T-Display timeout with esptool flash utility, double check power, and especially unplug it from any peripheral.

.. todolist::
  - Implement very low energy consumtion code to run on a 18650 battery.
  - Since a deepsleep resets the uc, it makes no sense to try to keep track of the cuckoo deviation. All of this should be removed and managed differently.
  - Review the scope of cfg, data and other mutable in the global scope
  - Implement gong count from the ULP (seems difficult since documentation is lacking)
  - Store the persistant data in RTC memory.

Done:
  - Implement proper function to log on terminal with datetime
  - Add fonts & text display or recompile with : https://github.com/russhughes/st7789_mpy
  - Display current time, cuckoo time, deviation, stop time on the display
  - Upgrade to MicroPython 1.13
"""

import config
import gc
from utime import time, localtime, mktime, sleep, sleep_ms, ticks_ms

import BlynkLib
import dht
import ntptime
from machine import ADC, PWM, RTC, Pin, SPI, Timer, reset, lightsleep, deepsleep, reset_cause, wake_reason
from machine import DEEPSLEEP_RESET, SOFT_RESET, HARD_RESET, PWRON_RESET, PIN_WAKE
import esp32
import ujson
import st7789
import vga2_8x16 as font_s
import vga2_bold_16x32 as font_b

# Constants
PRINT_DATETIME_LOG_TEMPLATE = "{0:04d}-{1:02d}-{2:02d} {3:02d}:{4:02d}:{5:02d} - {txt}"
TF = ['year', 'month', 'day', 'hour', 'minute', 'second', 'millisecond', 'microsecond'] # Time Format

# Vars
state = dict()

def log(txt):
    now = localtime()[:7]
    print((PRINT_DATETIME_LOG_TEMPLATE).format(*now, txt=txt))

class display:
    def __init__(self):
        self._tft = st7789.ST7789(
            SPI(2, baudrate=30000000, polarity=1, phase=1, sck=Pin(18), mosi=Pin(19)),
            135,
            240,
            reset=Pin(23, Pin.OUT),
            cs=Pin(5, Pin.OUT),
            dc=Pin(16, Pin.OUT),
            backlight=Pin(4, Pin.OUT),
            rotation=0)
        self._tft.init()

    def update_display(self, data={'time_deviation': 0, 'temperature': 0.0, 'humidity': 50.0, 'ip': '0.0.0.0'}):
        # Draw the title box
        self._tft.fill(st7789.BLACK)
        self._tft.rect(0,0,135,71,st7789.RED)
        self._tft.text(font_b, " Smart ", 10, 3, st7789.RED, st7789.BLACK)
        self._tft.text(font_b, " Cuckoo ", 1, 36, st7789.RED, st7789.BLACK)

        txtl = [
            "Date       {0:02d}-{1:02d}".format(*localtime()[1:3]),
            "NTP time   {0:02d}:{1:02d}".format(*localtime()[3:6]),
            "Cuckoo     {0:02d}:{1:02d}".format(*localtime(time() + data['time_deviation'])[3:6]),
            "Deviation {:02d} min.".format(data['time_deviation']//60),
            "Temperature {:2.1f}C".format(data['temperature']),
            "Humidity    {:2d} %".format(int(data['humidity'])),
            "Battery   {:2.2f} V".format(data['power']),
            "{}".format(data['ip']),
        ]

        # Print the data
        y = 71  # Vertical offset
        for txt in txtl:
            y += 4   # The line (above) is 1px
            self._tft.text(font_s, txt, 2, y)
            y += 17  # Font alone is 16px
            self._tft.hline(0, y, 134, st7789.RED)

class servo:
    """Manage the servo to block or restart the pendulum"""
    def __init__(self, cfg: object):
        self.servo = PWM(Pin(cfg.SERVO_PIN), freq=50)
        self.config = cfg
        self.angle = cfg.SERVO_MAX_ANGLE
        # We do not know if pendulum us running or not.
        # Restart it to be sure.
        #self.set_pendulum_running(False)
        #sleep(3)
        #self.set_pendulum_running(True)

    def __servo_angle(self, angle: int) -> int:
        """Convert angle into PWM ratio for the servo"""
        log("Servo angle: {}".format(angle))
        return int(angle/self.config.SERVO_MAX_ANGLE * (115 - 40)) + 40

    def set_angle(self, angle):
        """Angle setter"""
        self.angle = int(angle)
        self.servo.duty(
            self.__servo_angle(self.angle)
        )

    def get_angle(self) -> int:
        """Angle getter"""
        return self.angle

    def set_pendulum_running(self, run=True):
        """Start or stop the cuckoo clock"""
        # Get the sequence depending of the action (True=run/False=stop)
        log(("Start cuckoo" if run else "Stop cuckoo") + " pendulum")
        seq = self.config.CUCKOO_START if run == True else self.config.CUCKOO_STOP
        for angle in seq:
            self.set_angle(angle)
            sleep_ms(config.SERVO_STEP_EVERY_MS)
        self.pendulum_running = run
    
    def get_pendulum_running(self, run=True) -> bool:
        return self.pendulum_running

    def servo_deinit(self):
        """Stop PWM when closing (before sleep) to save battery"""
        self.servo.deinit()

class gong:
    """Manage the cuckoo gong switch used to get the time of the clock"""
    # Consider the following as personal documentation for time management on ESP32
    # utime.localtime()              = localtime tupple of the form (2020, 9, 27, 23, 27, 47, 6, 271)
    # utime.localtime( seconds )     = time tupple of epoch + int seconds
    # utime.time()                   = localtime as seconds since epoch (2020 01 01)
    # utime.mktime(date_as_tupple)   = inverse of localtime = seconds from epoch to date_as_tupple
    # (PRINT_DATETIME_TEMPLATE + " {t}").format(*(localtime()[:6]), t = "TEST")
    def __init__(self, cfg, pendulum_servo, init_gong_count):
        self.config = cfg
        self.gong_pin = Pin(cfg.GONG_PIN, Pin.IN, Pin.PULL_UP)
        self.rtc = RTC()
        self.pendulum_servo = pendulum_servo
        self.init_gong_count = init_gong_count
        self.gong_count = 0
        self.gong_count_timer = Timer(1)
        self.gong_lock_until = 0

        # Configure the gong_pin to both register a callback, and also wake from deepsleep
        self.gong_pin.irq(trigger=Pin.IRQ_FALLING, handler=self._gong_detected)
        esp32.wake_on_ext0(pin=self.gong_pin, level=esp32.WAKEUP_ALL_LOW)

    def _gong_detected(self, p):
        if ticks_ms() > self.gong_lock_until:
            self.gong_lock_until = ticks_ms() + 400  # Prevent contact bounces false counts by locking count for x ms
            self.gong_count += 1
            log("Gong {}".format(self.gong_count))
            # (Re)Set the timer that will detect last gong after timer overflow
            self.gong_count_timer.init(period=2000, mode=Timer.ONE_SHOT, callback=self._gongs_stopped)

    def _gongs_stopped(self, tmr):
        """Provides the total number of gongs after the call back timer expired"""
        if self.gong_count >= 2:
            # It is gong_count o'clock
            # Find the closest o'clock (+/-30m):
            # start from localtime
            dt = list(localtime())            # Let's use a mutable object for efficiency (list vs tuple) !
            # dt is of the form [2020, 10, 8, 17, 27, 33, 3, 282]
            if dt[TF.index('minute')] < 30:
                oclock_24 = dt[TF.index('hour')]
            else:
                oclock_24 = dt[TF.index('hour')] + 1
            pm = True if oclock_24 >= 13 else False   # Fixme: must take deviation or cuckoo time into account, not localtime

            # dt[TF.index('hour')] = self.gong_count + (12 if pm else 0)
            # cuckoo_time = dt[:TF.index('minute')] + [0] * 4
            # dt[TF.index('hour')] = self.gong_count + (12 if pm else 0)
            cuckoo_time = dt[:TF.index('hour')]                       # Keep only the date
            cuckoo_time.append(self.gong_count + (12 if pm else 0))   # Add the hour
            cuckoo_time.extend([0] * 4)                               # Zero the rest (m,s,ms,us)
            log(dt)
            log(cuckoo_time)

            # Calculate the clock deviation from ntp localtime (in seconds)
            data['time_deviation'] = (mktime(cuckoo_time) - mktime(dt)) 
            log("We're close to {} o'clock, so we are {}. Cuckoo just strikes {} o'clock so it's {} minutes {}".format(
                    str(oclock_24), "pm" if pm else "am", cuckoo_time[3], abs(data['time_deviation']//60) , "in advance" if data['time_deviation'] >= 0 else "late"
                )
            )

            # Fixme: Trigger display update

            # Fixme: Trigger a stop of the cuckoo pendulum for x seconds if more than 20s late.

            if self.gong_count + self.init_gong_count == self.config.CUCKOO_STOP_HOUR24 - (12 if pm else 0):
                # Let's stop the cuckoo
                self.pendulum_servo.set_pendulum_running(False)
                # Stop servo PWM to save battery
                # Fixme: This did not work, the servo is still trying to keep the angle. Solution might be to unpower the servo ?
                # self.pendulum_servo.servo.servo_deinit()
                # Let's sleep until 10m before wake up time to restart the pendulum on time.
                sleep_duration = 12 * 3600 + (mktime(cuckoo_time) - time()) - 600
                log("Sleeping for the next {} minutes.".format(sleep_duration//60))

                # Save the state
                with open('state.json', 'w') as f:
                    ujson.dump(state, f)
                sleep(5)  # Always sleep before deepsleep to ensure access from console
                deepsleep(sleep_duration * 1000) # deepsleep argument is in milliseconds

        # Reset Gong Counter
        self.gong_count = 0

class battery:
    def __init__(self, pin):
        self.adc = ADC(Pin(pin))
        self.adc.atten(ADC.ATTN_11DB)

    def get_voltage(self) -> float:
        """Provide power or battery voltage as float"""
        return self.adc.read()/4096*2*3.6

def measure_env(t):
    """Triggered periodically by timer to measure & send data from environment sensor"""
    d.measure()
    temperature, humidity = round(d.temperature(), 1), round(d.humidity(),1) 
    batt = battery(config.V_PIN)
    voltage = batt.get_voltage()
    log("Temperature: {t:2.1f}C \t Humidity: {h:2.0f}% \t Power: {v:1.2f}V".format(t=temperature, h=humidity, v=voltage))
    data['temperature'] = temperature
    data['humidity'] = humidity
    data['power'] = voltage
    disp.update_display(data)

    # The following typically fails if network or Blynk disconnected.
    try:
        blynk.virtual_write(0, temperature)
        blynk.virtual_write(3, humidity)
    except OSError:
        log("Blynk or WiFi probably disconnected. Resetting in 5s.")
        sleep(5)
        reset()

def set_ntp_time():
    """Set time from ntp, and adjust for local DST in central Europe (UTC+1 / UTC+2)"""
    def get_ntp_time():
        """Prevent crash if ntptime fails"""
        try:
            ntptime.settime()
        except (OSError, OverflowError) as e:
            log("Cannot get time. Probably WiFi issue. Resetting in 5s. Raised exception below:")
            log(e)
            sleep(5)
            reset()

    get_ntp_time()
    year = localtime()[0]
    # now  = ntptime.time() no need to call ntptime a second time since we just called it !
    now = time()
    HHMarch   = mktime((year, 3,(31-(int(5*year/4+4))%7),1,0,0,0,0,0)) #Time of March change to CEST
    HHOctober = mktime((year,10,(31-(int(5*year/4+1))%7),1,0,0,0,0,0)) #Time of October change to CET
    if now < HHMarch :                                                       # we are before last sunday of march
        ntptime.NTP_DELTA = 3155673600-3600                                  # CET:  UTC+1H
    elif now < HHOctober :                                                   # we are before last sunday of october
        ntptime.NTP_DELTA = 3155673600-7200                                  # CEST: UTC+2H
    else:                                                                    # we are after last sunday of october
        ntptime.NTP_DELTA = 3155673600-3600                                  # CET:  UTC+1H
    get_ntp_time()
        

#######################################
# Begin program execution from here   #
#######################################
# Check as soon as possible, before any possible other reset to not loose the reset cause
# P.S.: The soft_reset do not clear the reset reason
if reset_cause() == DEEPSLEEP_RESET:
    log("Starting after a Deep Sleep")
    # Just waking up from the about 12h deep sleep. Restart the pendulum.
    # Check the time we need to restart the cuckoo.
elif reset_cause() == HARD_RESET:
    log("Starting after a Hard Reset")
elif reset_cause() == SOFT_RESET:
    log("Starting after a Soft Reset")
elif reset_cause() == PWRON_RESET:
    log("Starting after a Power ON")
else:
    log("Last reset cause: {}".format(
        str(reset_cause())
    ))

rtc = RTC()
try:
    rtc_memory = ujson.loads(rtc.memory())
except ValueError:
    # rtc.memory() does not contain any valid JSON -> Recreate it
    rtc_memory = {'gong_count': 0}
    rtc.memory(ujson.dumps(rtc_memory))

# Extra precaution might not be required.
try:
    log("Gong Count : {}".format(rtc_memory['gong_count']))
except KeyError:
    log("gong_count key is missing . Creating it at 0")
    rtc_memory['gong_count'] = 0
    rtc.memory(ujson.dumps(rtc_memory))

init_gong_count = 0
if wake_reason() == PIN_WAKE:
    log("Wake reason : PIN_WAKE - woke up from a Cuckoo Gong")
    init_gong_count = 2  # we lost the first as it's the wake from deepsleep trigger, the second during boot
elif wake_reason() == 4:
    log("Wake reason 4 : Might be after an RTC reset.")
else:
    log("Wake reason : {}".format(str(wake_reason())))

if config.DEBUG:
    log("available objects : {}".format(str(dir())))

# Initialize Servo0 used to block or restart the clock pendulum (second initialization)
servo0 = servo(config)
# Initialize Gong switch
# It's important to initialize the gong early, when boot time is still predictable, to ensure
# we do not loose more than 2 gongs after wake up from Gong pin.
g = gong(config, servo0, init_gong_count)



data={'time_deviation': 0, 'temperature': 0.0, 'humidity': 50.0, 'ip': '0.0.0.0', 'power': 0.0}
# WiFi Connect:
data['ip'] = do_connect()[0]

# blynk fails to initialize when in the if __name__ == '__main__' condition. Strange.
try:
    blynk = BlynkLib.Blynk(config.AUTH_TOKEN)
except ValueError as e:
    log(e)
    sleep(5)
    reset()


disp = display()
disp.update_display(data)



if __name__ == '__main__':

    log("Fetch time from NTP servers")
    set_ntp_time()

    
    # Read the state dict:
    with open('state.json', 'r') as f:
        # dict.keys() is not implemented in Micropython. Let's work around.
        try:
            state = ujson.load(f)
            assert type(state['pendulum_running']) == type(bool())
        except Exception as e:
            # Something went wrong, maybe the file does not exist yet or is corrupted. Recreate it.
            state = {'pendulum_running': False}
            with open('state.json', 'w') as f:
                ujson.dump(state, f)
    if state['pendulum_running'] == False:
        now_minutes = localtime()[TF.index('minute')] 
        next_oclock_in_ms = ( 60 - now_minutes - 1 ) * 60 * 1000 # Restart pendulum 1 minute in advance.
        if now_minutes <= 57:
            # Too long to wait.. go back to deep sleep for few minutes.
            deepsleep(next_oclock_in_ms)    
        if now_minutes <= 59:
            sleep_ms(next_oclock_in_ms)    # Restart pendulum with 1 minute advance.
        log("It's time to wake up from a good night of sleep cuckoo clock !")
        servo0 = servo(config.SERVO_PIN, config)
        # Make sure it's not running:
        servo0.set_pendulum_running(False)
        sleep(3)
        # (Re)start it
        servo0.set_pendulum_running(True)
    
    @blynk.on('readV2')
    def v2_read_handler():
        """uptime in minutes since last reset """
        blynk.virtual_write(2, ticks_ms() // 1000 // 60)

    @blynk.on('readV4')
    def v4_read_handler():
        """Get free memory from garbage collector"""
        blynk.virtual_write(4, gc.mem_free() // 1024)

    @blynk.on('V1')
    def servo_handler(value):
        """Servo simple command"""
        servo0.set_angle(value[0])

    @blynk.on('V5')
    def cuckoo_handler(value):
        servo0.set_pendulum_running(bool(int(value[0])))
    
    log("Connecting to Blynk and register call-backs...")
    # Initialize DHT Temp & Humidity sensor
    d = dht.DHT22(Pin(config.DHT22_PIN))
    retry = 3
    while retry < 3:
        sleep(1)
        try:
            d.measure()
            break
        except OSError:
            retry += 1
            print(".", end="")
        print("")
        if retry == 3:
            log("Unable to read temperature from DHT22 after 3 attempts, resetting in 5s")
            sleep(5)
            reset()


    # Measure once, then initialize timer to periodically measure temp & humidity
    measure_env(None)
    measure_env_timer = Timer(0)
    measure_env_timer.init(period=30000, mode=Timer.PERIODIC, callback=measure_env)

    log("Running Blynk")
    while True:
        try:
            blynk.run()
        except (KeyboardInterrupt, RuntimeError) as e:
            # Stop the timer before exit
            measure_env_timer.deinit()
            raise e
