# boot.py - - runs on boot-up
from time import sleep
import config
import network

network.phy_mode(network.MODE_11B)

def do_connect():
    sta_if = network.WLAN(network.STA_IF)
    if not sta_if.isconnected():
        print('connecting to network...')
        sta_if.active(True)
        sta_if.connect(config.WIFI_SSID, config.WIFI_PASS)
        i = 0
        while not sta_if.isconnected() and i < 15:
            print("Connecting...")
            sleep(2)
            i += 1
    print('network config:', sta_if.ifconfig())

    # Ensure WebREPL is available when network is.
    import webrepl
    webrepl.start()
    return(sta_if.ifconfig())

