# Configuration file
# Adapt to your needs, and rename the file to config.py
AuthToken = 'xxxxxxxx_xxxxxxx_xxxxx-xxx-xxxxx'
WIFI_SSID = 'wifissid'
WIFI_PASS = 'wifipass'
DHT22_PIN = 2
SERVO_PIN = 27
SERVO_MAX_ANGLE = 160

COUCOU_STOP_HOUR24 = 20 # Stop the coucou when it's 8pm
COUCOU_STOP = [ 90, 80, 70, 60, 55, 50, 45, 40, 40, 70 ]
COUCOU_STOP_SPEED = 2
COUCOU_START = [ 70, 0, SERVO_MAX_ANGLE ]
COUCOU_START_SPEED = 100
GONG_PIN = 15